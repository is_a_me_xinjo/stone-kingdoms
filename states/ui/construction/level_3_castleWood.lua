local el, backButton, destroyButton, getCostAndType = ...

local states = require("states.ui.states")
local ActionBarButton = require("states.ui.ActionBarButton")
local ActionBar = require("states.ui.ActionBar")
local Events = require("objects.Enums.Events")

local woodenWallButton = ActionBarButton:new(love.graphics.newImage("assets/ui/fortifications/wooden/wooden_wall_ab.png"),
    states.STATE_INGAME_CONSTRUCTION, 1, true, nil)
woodenWallButton:setOnClick(
    function(self)
        ActionBar:selectButton(woodenWallButton)
        _G.BuildController:set(
            "WoodenWall", function()
                woodenWallButton:unselect()
            end)
    end)

woodenWallButton:setOnUnselect(function()
    local WallController = require("objects.Controllers.WallController")
    WallController.clicked = false
end)


local walkableWoodenWallButton = ActionBarButton:new(love.graphics.newImage("assets/ui/fortifications/wooden/wooden_wall_walkable_ab.png"),
    states.STATE_INGAME_CONSTRUCTION, 2, false, nil)
walkableWoodenWallButton:setOnClick(
    function(self)
        ActionBar:selectButton(walkableWoodenWallButton)
        _G.BuildController:set(
            "WalkableWoodenWall", function()
                walkableWoodenWallButton:unselect()
            end)
    end)
walkableWoodenWallButton:setOnUnselect(function()
    local WallController = require("objects.Controllers.WallController")
    WallController.clicked = false
end)

local woodenTowerButton = ActionBarButton:new(love.graphics.newImage("assets/ui/fortifications/wooden/wooden_tower.png"),
    states.STATE_INGAME_CONSTRUCTION, 3, false, nil)
woodenTowerButton:setOnClick(
    function(self)
        _G.BuildController:set(
            "WoodenTower", function()
                woodenTowerButton:unselect()
            end)
        ActionBar:selectButton(woodenTowerButton)
    end)

local woodenPermimeterTowerButton = ActionBarButton:new(love.graphics.newImage("assets/ui/fortifications/wooden/wooden_perimeter_tower.png"),
    states.STATE_INGAME_CONSTRUCTION, 4, false, nil)
    woodenPermimeterTowerButton:setOnClick(
    function(self)
        _G.BuildController:set(
            "WoodenPerimeterTower", function()
                woodenPermimeterTowerButton:unselect()
            end)
        ActionBar:selectButton(woodenPermimeterTowerButton)
    end)

local woodenDefenseTowerButton = ActionBarButton:new(love.graphics.newImage("assets/ui/fortifications/wooden/wooden_defense_tower.png"),
    states.STATE_INGAME_CONSTRUCTION, 5, false, nil)
    woodenDefenseTowerButton:setOnClick(
    function(self)
        _G.BuildController:set(
            "WoodenDefenseTower", function()
                woodenDefenseTowerButton:unselect()
            end)
        ActionBar:selectButton(woodenDefenseTowerButton)
    end)

local woodenGateEastButton = ActionBarButton:new(love.graphics.newImage("assets/ui/fortifications/wooden/wooden_gate_east.png"),
    states.STATE_INGAME_CONSTRUCTION, 6, false, nil)
woodenGateEastButton:setOnClick(
    function(self)
        _G.BuildController:set(
            "WoodenGateEast", function()
                woodenGateEastButton:unselect()
            end)
        ActionBar:selectButton(woodenGateEastButton)
    end)

local woodenGateSouthButton = ActionBarButton:new(love.graphics.newImage("assets/ui/fortifications/wooden/wooden_gate_south.png"),
    states.STATE_INGAME_CONSTRUCTION, 7, false, nil)
woodenGateSouthButton:setOnClick(
    function(self)
        _G.BuildController:set(
            "WoodenGateSouth", function()
                woodenGateSouthButton:unselect()
            end)
        ActionBar:selectButton(woodenGateSouthButton)
    end)

local woodenGateEastBigButton = ActionBarButton:new(love.graphics.newImage("assets/ui/fortifications/wooden/wooden_gate_east_big.png"),
    states.STATE_INGAME_CONSTRUCTION, 8, false, nil)
    woodenGateEastBigButton:setOnClick(
    function(self)
        _G.BuildController:set(
            "WoodenGateEastBig", function()
                woodenGateEastBigButton:unselect()
            end)
        ActionBar:selectButton(woodenGateEastBigButton)
    end)

local woodenGateSouthBigButton = ActionBarButton:new(love.graphics.newImage("assets/ui/fortifications/wooden/wooden_gate_south_big.png"),
    states.STATE_INGAME_CONSTRUCTION, 9, false, nil)
    woodenGateSouthBigButton:setOnClick(
    function(self)
        _G.BuildController:set(
            "WoodenGateSouthBig", function()
                woodenGateSouthBigButton:unselect()
            end)
        ActionBar:selectButton(woodenGateSouthBigButton)
    end)

local buildings = {
    { button = walkableWoodenWallButton,    name = "WalkableWoodenWall",    description = "A defensive wall made that is walkable on the top.",      tier = 1 },
    { button = woodenTowerButton,           name = "WoodenTower",           description = "A wooden tower that is missing some stairs apparently.",  tier = 1 },
    { button = woodenGateEastButton,        name = "WoodenGateEast",        description = "A wooden gate that can let friendly units pass through.", tier = 1 },
    { button = woodenGateSouthButton,       name = "WoodenGateSouth",       description = "A wooden gate that can let friendly units pass through.", tier = 1 },
    { button = woodenGateEastBigButton,     name = "WoodenGateEastBig",     description = "A wooden gate that can let friendly units pass through.", tier = 1 },
    { button = woodenGateSouthBigButton,    name = "WoodenGateSouthBig",    description = "A wooden gate that can let friendly units pass through.", tier = 1 },
    { button = woodenPermimeterTowerButton, name = "WoodenPerimeterTower",  description = "A wooden tower that is missing some stairs apparently.",  tier = 1 },
    { button = woodenDefenseTowerButton,    name = "WoodenDefenseTower",    description = "A wooden tower that is missing some stairs apparently.",  tier = 1 },
    { button = woodenWallButton,            name = "WoodenWall",            description = "A defensive wall made from sharpened tree trunks.",       tier = 1 }
}

local function displayTooltips()
    if ActionBar:getCurrentGroup() ~= "woodenBuildings" then return end
    for _, building in ipairs(buildings) do
        local tooltipText = getCostAndType(building.name, building.description)
        building.button:setTooltip(building.name, tooltipText)
        if building.tier <= _G.state.tier then
            building.button:enable()
        else
            building.button:disable()
            building.button:setTooltip("You need to upgrade your keep to build this")
        end
    end
end

_G.bus.on(Events.OnResourceStore, displayTooltips)
_G.bus.on(Events.OnResourceTake, displayTooltips)
_G.bus.on(Events.OnGoldChanged, displayTooltips)
_G.bus.on(Events.OnTierUpgraded, displayTooltips)

el.buttons.woodenBuildings:setOnClick(function(self)
    ActionBar:showGroup("woodenBuildings", _G.fx["metpush15"])
    displayTooltips()
end)


ActionBar:registerGroup("woodenBuildings",
    { woodenWallButton, walkableWoodenWallButton, woodenTowerButton, woodenGateEastButton,
        woodenGateSouthButton, woodenGateEastBigButton,  woodenGateSouthBigButton, woodenPermimeterTowerButton, woodenDefenseTowerButton, backButton, destroyButton })
