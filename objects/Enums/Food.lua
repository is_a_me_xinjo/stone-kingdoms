---@enum food
local FOOD = {
    apples = "apples",
    bread = "bread",
    cheese = "cheese",
    meat = "meat"
}

return FOOD
